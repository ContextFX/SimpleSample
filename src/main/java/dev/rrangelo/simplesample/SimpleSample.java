/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package dev.rrangelo.simplesample;

/**
 *
 * @author Ramon Rangel Osorio <ramon.rangel@protonmail.com>
 */
public class SimpleSample {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        SimpleSampleApp.main(args);
    }
    
}
