/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 */

package dev.rrangelo.simplesample;

import javafx.application.Application;
import javafx.stage.Stage;
import org.contextfx.reactive.ReactiveContextApplication;
import org.contextfx.reactive.components.views.ReactiveManager;

/**
 *
 * @author Ramon Rangel Osorio <ramon.rangel@protonmail.com>
 */
public class SimpleSampleApp extends Application {

    public static void main(String[] args) {
        launch(args);
    }

    @Override
    public void start(Stage stage) throws Exception {
        ReactiveContextApplication context = new ReactiveContextApplication(SimpleSampleApp.class);
        stage.setTitle("ContextFX Sample");
        stage.setHeight(600);
        stage.setWidth(800);
        ReactiveManager.getRactiveManager(context.getReactiveContainer().getComponents()).init(stage, SimpleSampleApp.class);
    }
}
