/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dev.rrangelo.simplesample.controllers;

import dev.rrangelo.simplesample.services.CounterService;
import org.contextfx.reactive.annotations.ReactiveController;
import org.contextfx.reactive.annotations.ReactiveInjectable;

/**
 *
 * @author Ramon Rangel Osorio <ramon.rangel@protonmail.com>
 */
@ReactiveController
public class CounterController {
    
    @ReactiveInjectable
    private CounterService service;
    
    public void count() {
        service.count();
    }
    
    public void clean() {
        service.clean();
    }
    
}
