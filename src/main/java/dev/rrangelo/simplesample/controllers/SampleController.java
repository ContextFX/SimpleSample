/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/javafx/FXMLController.java to edit this template
 */
package dev.rrangelo.simplesample.controllers;

import org.contextfx.reactive.annotations.ReactiveController;
import org.contextfx.reactive.components.views.ReactiveManager;

/**
 * FXML Controller class
 *
 * @author Ramon Rangel Osorio <ramon.rangel@protonmail.com>
 */
@ReactiveController
public class SampleController {

    public void redirect() {
        ReactiveManager.getRactiveManager().switchView("CounterView");
    }
}
