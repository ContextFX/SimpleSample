/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dev.rrangelo.simplesample.models;

import org.contextfx.reactive.annotations.ReactiveModel;
import org.reactfx.value.Val;
import org.reactfx.value.Var;

/**
 *
 * @author Ramon Rangel Osorio <ramon.rangel@protonmail.com>
 */
@ReactiveModel
public class CounterModel {
    
    private Var<Integer> count;
    
    public CounterModel() {
        
        count = Var.newSimpleVar(0);
        
    }
    
    public void setCount(Integer count) {
        this.count.setValue(count);
    }
    
    public Val<Integer> getCount() {
        return count;
    }
    
}
