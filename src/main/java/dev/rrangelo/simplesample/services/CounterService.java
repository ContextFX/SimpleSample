/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dev.rrangelo.simplesample.services;

import dev.rrangelo.simplesample.models.CounterModel;
import org.contextfx.reactive.annotations.ReactiveInjectable;
import org.contextfx.reactive.annotations.ReactiveService;

/**
 *
 * @author Ramon Rangel Osorio <ramon.rangel@protonmail.com>
 */
@ReactiveService
public class CounterService {
    
    @ReactiveInjectable
    private CounterModel model;
    
    public void count() {
        model.setCount(model.getCount().getValue() + 1);
    }
    
    public void clean() {
        model.setCount(0);
    }
    
}
