/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dev.rrangelo.simplesample.views;

import dev.rrangelo.simplesample.controllers.CounterController;
import dev.rrangelo.simplesample.models.CounterModel;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import org.contextfx.reactive.annotations.ReactiveView;
import org.contextfx.reactive.components.ReactiveContainer;
import org.reactfx.EventStreams;

/**
 *
 * @author Ramon Rangel Osorio <ramon.rangel@protonmail.com>
 */
@ReactiveView
public class CounterView implements Initializable {

    @FXML
    private Label message;
    
    @FXML
    private Button count;
    
    @FXML
    private Button clean;
    
    private CounterController controller;
    
    private CounterModel model;

    public CounterView() {
        model = (CounterModel) ReactiveContainer.getComponent(CounterModel.class);
        controller = (CounterController) ReactiveContainer.getComponent(CounterController.class);
    }
    
    @Override
    public void initialize(URL url, ResourceBundle rb) {
        
        message.setText("You're clicked 0 times");
        count.setText("Count");
        clean.setText("Clean");
        
        model.getCount().changes()
                .subscribe(e -> message.setText("You're clicked " + e.getNewValue() + " times"));
        
        
        EventStreams.eventsOf(count, ActionEvent.ACTION)
                .subscribe(event -> controller.count());
        
        EventStreams.eventsOf(clean, ActionEvent.ACTION)
                .subscribe(event -> controller.clean());
    }
    
}
  