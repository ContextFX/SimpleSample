/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package dev.rrangelo.simplesample.views;

import dev.rrangelo.simplesample.controllers.SampleController;
import java.net.URL;
import java.util.ResourceBundle;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import org.contextfx.annotations.Injectable;
import org.contextfx.reactive.annotations.ReactiveView;
import org.contextfx.reactive.components.ReactiveContainer;
import org.reactfx.EventStreams;

/**
 *
 * @author Ramon Rangel Osorio <ramon.rangel@protonmail.com>
 */
@ReactiveView(main = true)
public class SampleView implements Initializable {
    
    @FXML
    private Label greet;
    
    @FXML
    private Button redirect;
    
    @Injectable
    private SampleController controller;

    @Override
    public void initialize(URL url, ResourceBundle rb) {
        controller = (SampleController) ReactiveContainer.getComponent(SampleController.class);
        
        greet.setText("Hello World from JavaFX with ControlsFX, ReactFX and ContextFX");
        redirect.setText("Let's go to counter!");
        
        EventStreams.eventsOf(redirect, ActionEvent.ACTION)
                .subscribe(event -> controller.redirect());
        
    }
    
}
